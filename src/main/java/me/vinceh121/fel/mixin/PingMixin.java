package me.vinceh121.fel.mixin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.network.ClientConnection;
import net.minecraft.network.packet.c2s.query.QueryRequestC2SPacket;
import net.minecraft.server.network.ServerQueryNetworkHandler;

@Mixin(ServerQueryNetworkHandler.class)
public class PingMixin {
	private static final Logger LOG = LogManager.getLogger(PingMixin.class);

	@Shadow
	private ClientConnection connection;

	@Inject(at = @At("HEAD"), method = "onRequest")
	private void onRequest(QueryRequestC2SPacket packet, CallbackInfo info) {
		LOG.info("{} is pinging the server", connection == null ? connection : connection.getAddress());
	}
}
