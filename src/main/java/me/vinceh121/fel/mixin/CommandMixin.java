package me.vinceh121.fel.mixin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;

@Mixin(CommandManager.class)
public class CommandMixin {
	private static final Logger LOG = LogManager.getLogger(CommandMixin.class);

	@Inject(at = @At("HEAD"), method = "execute")
	private void execute(ServerCommandSource commandSource, String command, CallbackInfoReturnable<Integer> info) {
		LOG.info("{} executed `{}`", commandSource.getName(), command);
	}
}
